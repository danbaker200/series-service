Series Service
=====

### Environment setup

* Install Vagrant (http://www.vagrantup.com/downloads)
* Install Virtualbox (https://www.virtualbox.org/wiki/Downloads)


### Start the development environment
* vagrant up
* vagrant ssh
* npm install

### Run the app (defaults to port 3000)
npm run start

### Run the tests
npm run test