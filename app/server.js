'use strict';

import express from 'express';
import bodyParser from 'body-parser';
import SeriesTransformer from './seriesTransformer';

export default function() {
    let app = express();
    app.set('port', process.env.PORT || 3000);

    let errorHandler = function(err, req, res, next) {
        res.status(400).send({
            error: "Could not decode request: JSON parsing failed"
        });
    };

    app.use(bodyParser.json());
    app.use(errorHandler);

    app.get('/', (req, res) => {
        res.send('ok')
    });

    app.post('/', (req, res) => {
        if (req.body && req.body.payload && req.body.payload instanceof Array) {
            let seriesData = new SeriesTransformer(req.body.payload)
                .withDrm()
                .withEpisodes()
                .transform();

            res.json({
                response: seriesData
            });
        } else {
            errorHandler(null, req, res);
        }
    });

    app.use(errorHandler);

    return app.listen(app.get('port'), () => {
        console.log('started at port: ' + app.get('port'));
    });
}
