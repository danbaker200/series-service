'use strict';

import _get from 'lodash/object/get';

class SeriesTransformer {

    constructor(seriesList) {
        this.seriesList = seriesList
    }

    withDrm() {
        this.seriesList = this.seriesList.filter((series) => {
            return series.drm;
        });
        return this;
    }

    withEpisodes() {
        this.seriesList = this. seriesList.filter((series) => {
            return series.episodeCount && series.episodeCount > 0
        });
        return this;
    }

    transform() {
        return this.seriesList.map((series) => {
            return {
                image: _get(series, 'image.showImage'),
                slug: series.slug,
                title: series.title
            }
        });
    }
}

export default SeriesTransformer;