"use strict";

import server from '../../app/server';
import request from 'superagent';
import sampleRequest from './sampleRequest.json';
import sampleResponse from './sampleResponse.json';

describe('Series Service', () => {
    let baseUrl = 'http://127.0.0.1:3000/';
    var app;

    before(function() {
        app = server();
    });

    after(function() {
        app.close();
    });

    it('produces the correct output', (done) => {
        request.post(baseUrl)
            .send(sampleRequest)
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .end(function(err, res){
                expect(res.statusCode).to.equal(200);

                var json = JSON.parse(res.text);
                expect(json).to.deep.equals(sampleResponse);
                done();
            });
    });

    it('returns a 400 error when invalid json is supplied', (done) => {
        request.post(baseUrl)
            .send('invalid json')
            .set('Accept', 'application/json')
            .set('Content-Type', 'application/json')
            .end(function(err, res){
                expect(res.statusCode).to.equal(400);

                var json = JSON.parse(res.text);
                expect(json).to.deep.equals({"error": "Could not decode request: JSON parsing failed"});
                done();
            });
    });

});