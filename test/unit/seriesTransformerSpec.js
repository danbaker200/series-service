"use strict";

import SeriesTransformer from '../../app/seriesTransformer';

describe('Series Transformer', () => {

    var seriesTransformer;
    let testData = [{
            "drm": true,
            "episodeCount": 0
        },
        {
            "drm": false,
            "episodeCount": 2
        },
        {
            "drm": true,
            "episodeCount": 4,
            "image": {
                "showImage": "http://catchup.ninemsn.com.au/img/jump-in/shows/ScoobyDoo1280.jpg"
            },
            "slug": "show/scoobydoomysteryincorporated",
            "title": "Scooby-Doo! Mystery Incorporated"
        }
    ]

    beforeEach(() => {
        seriesTransformer = new SeriesTransformer(testData);
    });

    it('filters out series without drm', () => {
        expect(seriesTransformer.withDrm().transform().length).to.equals(2);
    });

    it('filters out series without episodes', () => {
        expect(seriesTransformer.withEpisodes().transform().length).to.equals(2);
    });

    it('filters out series without episodes and drm', () => {
        expect(seriesTransformer.withDrm().withEpisodes().transform().length).to.equals(1);
    });

    it('transforms slug correctly', () => {
        let result = seriesTransformer.withDrm().withEpisodes().transform()[0];
        expect(result.slug).to.equals('show/scoobydoomysteryincorporated');
    });

    it('transforms title correctly', () => {
        let result = seriesTransformer.withDrm().withEpisodes().transform()[0];
        expect(result.title).to.equals('Scooby-Doo! Mystery Incorporated');
    });

    it('transforms image correctly', () => {
        let result = seriesTransformer.withDrm().withEpisodes().transform()[0];
        expect(result.image).to.equals('http://catchup.ninemsn.com.au/img/jump-in/shows/ScoobyDoo1280.jpg');
    });

});